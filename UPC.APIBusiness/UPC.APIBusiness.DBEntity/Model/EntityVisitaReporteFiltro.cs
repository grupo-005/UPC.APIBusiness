﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityVisitaReporteFiltro
    {
        public Boolean filtro { get; set; }

        public DateTime fecha_desde { get; set; }
        public DateTime fecha_hasta { get; set; }
    }
}
