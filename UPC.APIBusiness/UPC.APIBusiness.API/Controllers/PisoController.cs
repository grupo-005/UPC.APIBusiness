﻿using DBContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{
    [Route("api/piso")]
    [ApiController]
    public class PisoController : Controller
    {

        protected readonly IPisoRepository _pisoRepository;

        public PisoController(IPisoRepository pisoRepository)
        {
            _pisoRepository = pisoRepository;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("listar")]
        public ActionResult GetAll()
        {
            var ret = _pisoRepository.Listar();
            return Json(ret);
        }

    }
}
