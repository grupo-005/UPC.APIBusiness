﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityFirmaEspacio: EntityBase
    {
        public int id_firma_espacio { get; set; }
        public int id_visita { get; set; }
        public int id_piso { get; set; }
        public int id_cubiculo { get; set; }

        public int id_asiento { get; set; }
        public int id_lado { get; set; }
    }
}
