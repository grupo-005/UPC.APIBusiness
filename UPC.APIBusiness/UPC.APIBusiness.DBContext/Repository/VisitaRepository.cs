﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class VisitaRepository : BaseRepository, IVisitaRepository
    {
        public EntityBaseResponse insertarVisita(EntityVisita visita)
        {

            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {

                    string insertQuery = @"INSERT INTO [dbo].[registro_visita]([id_persona], [id_linea], [fecha_desde], [fecha_hasta], [estado],[estado_registro], [fecha_registro], [usuario_crea]) VALUES (@id_persona, @id_linea, @fecha_desde, @fecha_hasta, @estado, @estado_registro, @fecha_registro, @usuario_crea)";

                    int result = db.Execute(insertQuery, visita);

                    if (result > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Registro exitoso!";
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Error en el registro!";
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;

        }

        public EntityBaseResponse listarSolicitudes()
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    List<EntitySolicitudEspacio> list = new List<EntitySolicitudEspacio>();

                    const string sql = "select rv.id_visita ,p.nombre , p.apellido , ls.nombre_linea, p.numero_documento  , rv.fecha_desde , rv.fecha_hasta, rv.estado , fe.id_firma_espacio from registro_visita rv inner join  persona p on rv.id_persona = p.id_persona inner join linea_servicio ls on rv.id_linea = ls.id_linea left join firma_espacio fe ON fe.id_visita = rv.id_visita and fe.estado_registro = 1 order by rv.id_linea,rv.fecha_desde ,rv.estado ASC";

                    list = db.Query<EntitySolicitudEspacio>(sql).ToList();

                    if (list.Count > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = list;
                    }
                    else
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = "no se encontró dato";
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }

        public EntityBaseResponse listarVisitasReporte(EntityVisitaReporteFiltro visita)
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    List<EntityReporteVisita> list = new List<EntityReporteVisita>();

                    string sql = @"select rv.id_visita ,p.nombre , p.apellido , ls.nombre_linea, p.numero_documento  , rv.fecha_desde , rv.fecha_hasta, rv.estado , fe.id_firma_espacio , fe.id_piso, p2.nomenclatura as nomenclaturapiso, p2.descripcion as descripcionpiso , fe.id_cubiculo, c.nomenclatura as nomenclaturacubiculo, c.descripcion as descripcioncubiculo , fe.id_lado , l.nomenclatura as nomenclaturalado, l.descripcion as descripcionlado , fe.id_asiento, a.nomenclatura as nomenclaturaasiento, a.descripcion as descripcionasiento from registro_visita rv inner join  persona p on rv.id_persona = p.id_persona inner join linea_servicio ls on rv.id_linea = ls.id_linea left join firma_espacio fe ON fe.id_visita = rv.id_visita and fe.estado_registro = 1 left join piso p2 on p2.id_piso = fe.id_piso left join cubiculo c on c.id_cubiculo = fe.id_cubiculo left join lado l on l.id_lado = fe.id_lado left join asiento a on a.id_asiento = fe.id_asiento where fe.id_firma_espacio > 0 order by rv.id_linea,rv.fecha_desde ,rv.estado ASC";


                    if (visita.filtro && visita.fecha_desde != null && visita.fecha_hasta != null)
                    {
                        sql = "select rv.id_visita ,p.nombre , p.apellido , ls.nombre_linea, p.numero_documento  , rv.fecha_desde , rv.fecha_hasta, rv.estado , fe.id_firma_espacio , fe.id_piso, p2.nomenclatura as nomenclaturapiso, p2.descripcion as descripcionpiso , fe.id_cubiculo, c.nomenclatura as nomenclaturacubiculo, c.descripcion as descripcioncubiculo , fe.id_lado , l.nomenclatura as nomenclaturalado, l.descripcion as descripcionlado , fe.id_asiento, a.nomenclatura as nomenclaturaasiento, a.descripcion as descripcionasiento from registro_visita rv inner join  persona p on rv.id_persona = p.id_persona inner join linea_servicio ls on rv.id_linea = ls.id_linea left join firma_espacio fe ON fe.id_visita = rv.id_visita and fe.estado_registro = 1 left join piso p2 on p2.id_piso = fe.id_piso left join cubiculo c on c.id_cubiculo = fe.id_cubiculo left join lado l on l.id_lado = fe.id_lado left join asiento a on a.id_asiento = fe.id_asiento where fe.id_firma_espacio > 0 and (rv.fecha_desde >= @fecha_desde and rv.fecha_hasta <= @fecha_hasta) order by rv.id_linea,rv.fecha_desde ,rv.estado ASC";

                        list = db.Query<EntityReporteVisita>(sql, visita).ToList();

                    }
                    else {
                        list = db.Query<EntityReporteVisita>(sql).ToList();
                    }

                    

                    if (list.Count > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = list;
                    }
                    else
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = "no se encontró dato";
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }

        public EntityBaseResponse validarRangoFecha(EntityVisitaRangofecha visita)
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    List<EntityVisita> list = new List<EntityVisita>();

                    const string sql = "select *  from registro_visita where id_linea =@id_linea and (fecha_desde BETWEEN @fecha_desde and @fecha_hasta or fecha_hasta BETWEEN  @fecha_desde and @fecha_hasta) and estado_registro = 1 and estado in (0,1)";

                    list = db.Query<EntityVisita>(sql, new { id_linea = visita.id_linea, fecha_desde = visita.fecha_desde, fecha_hasta = visita.fecha_hasta }).ToList();

                    if (list.Count>0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = list;
                    }
                    else
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = "no se encontró dato";
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }
    }
}
