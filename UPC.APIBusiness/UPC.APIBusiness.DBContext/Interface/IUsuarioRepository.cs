﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface IUsuarioRepository
    {
        EntityBaseResponse GetUsuario(string usuario, string password);
    }
}
