﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityLineaServicio: EntityBase
    {
        public int id_linea { get; set; }
        public string nombre_linea { get; set; }
    }
}
