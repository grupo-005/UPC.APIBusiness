﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityAsiento: EntityBase
    {

        public int id_asiento { get; set; }
        public string descripcion { get; set; }
        public string nomenclatura { get; set; }
        public string valor { get; set; }

    }
}
