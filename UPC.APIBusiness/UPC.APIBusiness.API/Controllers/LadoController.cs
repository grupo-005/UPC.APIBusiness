﻿using DBContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{
    [Route("api/lado")]
    [ApiController]
    public class LadoController : Controller
    {
        protected readonly ILadoRepository _ladoRepository;

        public LadoController(ILadoRepository ladoRepository)
        {
            _ladoRepository = ladoRepository;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("listar")]
        public ActionResult GetAll()
        {
            var ret = _ladoRepository.Listar();
            return Json(ret);
        }

    }
}
