﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class RegistroVacunaRepository : BaseRepository, IRegistroVacunaRepository
    {
        public EntityBaseResponse insertRegistroVacuna(EntityRegistroVacuna vacuna)
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {

                    string insertQuery = @"INSERT INTO [dbo].[registro_vacuna]([vacunado], [numero_dosis], [id_tipo_vacuna], [descripcion_otro_tipo], [fecha_vacunacion],[lote], [id_persona], [estado_registro], [fecha_registro], [usuario_crea]) VALUES (@vacunado, @numero_dosis, @id_tipo_vacuna, @descripcion_otro_tipo, @fecha_vacunacion, @lote, @id_persona, @estado_registro, @fecha_registro, @usuario_crea)";

                    int result = db.Execute(insertQuery, vacuna);

                    if (result > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Registro exitoso!";
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Error en el registro!";
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }

        public EntityBaseResponse listarReporte(EntityReporteVacunadoFiltro filtro)
        {

            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    List<EntityReporteVacunado> list = new List<EntityReporteVacunado>();

                    string sql = "select p.id_persona, p.numero_documento , p.nombre , p.apellido, rv.id_registro_vacuna,rv.vacunado, rv.numero_dosis, rv.descripcion_otro_tipo,rv.fecha_vacunacion, rv.lote, tv.descripcion from persona p left join registro_vacuna rv on p.id_persona = rv.id_persona left join tipo_vacuna tv  on rv.id_tipo_vacuna = tv.id_tipo_vacuna where rv.id_registro_vacuna > 0 order by rv.fecha_vacunacion DESC; ";


                    if (filtro.filtro)
                    {
                        sql = "select p.id_persona, p.numero_documento , p.nombre , p.apellido, rv.id_registro_vacuna,rv.vacunado, rv.numero_dosis, rv.descripcion_otro_tipo,rv.fecha_vacunacion, rv.lote, tv.descripcion from persona p left join registro_vacuna rv on p.id_persona = rv.id_persona left join tipo_vacuna tv  on rv.id_tipo_vacuna = tv.id_tipo_vacuna where rv.id_registro_vacuna > 0 and (p.nombre like @nombre or p.apellido like @apellido or p.numero_documento  like @numeroDoc) order by rv.fecha_vacunacion DESC; ";
                        
                        list = db.Query<EntityReporteVacunado>(sql, new { nombre = (filtro.nombre != null ? "%" + filtro.nombre + "%" : ""), apellido = (filtro.apellido!=null?"%" + filtro.apellido + "%":""), numeroDoc = (filtro.numeroDoc!=null?"%" + filtro.numeroDoc + "%":"") }).ToList();

                    }
                    else
                    {
                        list = db.Query<EntityReporteVacunado>(sql).ToList();
                    }



                    if (list.Count > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = list;
                    }
                    else
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = "no se encontró dato";
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;

        }

        public EntityBaseResponse obtenerRegistroVacuna(int id)
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    var result = new EntityRegistroVacuna();

                    const string sql = "select * from registro_vacuna where id_registro_vacuna = @idVacuna";

                    result = db.Query<EntityRegistroVacuna>(sql, new { idVacuna = id }).FirstOrDefault();

                    if (result != null)
                    {

                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = result;
                    }
                    else
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = "no hay data!";
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }

        public EntityBaseResponse obtenerRegistroVacunaByPersona(int id)
        {

            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    var result = new EntityRegistroVacuna();

                    const string sql = "select * from registro_vacuna where id_persona = @idPersona";

                    result = db.Query<EntityRegistroVacuna>(sql, new { idPersona = id }).FirstOrDefault();

                    if (result != null)
                    {

                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = result;
                    }
                    else
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = "no hay data!";
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;

        }
    }
}
