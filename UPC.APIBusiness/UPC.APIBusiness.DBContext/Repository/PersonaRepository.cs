﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class PersonaRepository : BaseRepository, IPersonaRepository
    {
        public EntityBaseResponse insertPersona(EntityPersona persona)
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {

                    string insertQuery = @"INSERT INTO [dbo].[persona]([nombre], [apellido], [celular], [numero_documento], [fecha_nacimiento], [direccion], [estado_registro], [fecha_registro], [usuario_crea], [usuario_modifica]) VALUES (@nombre, @apellido, @celular, @numero_documento, @fecha_nacimiento, @direccion, @estado_registro, @fecha_registro, @usuario_crea, @usuario_modifica)";

                    if (persona.id_persona > 0) {
                        insertQuery = @"UPDATE [dbo].[persona] SET [nombre]=@nombre, [apellido]=@apellido, [celular]=@celular, [numero_documento]=@numero_documento, [fecha_nacimiento]=@fecha_nacimiento, [direccion]=@direccion, [estado_registro]=@estado_registro, [fecha_actualizacion]=@fecha_actualizacion, [usuario_modifica]=@usuario_modifica WHERE [id_persona]=@id_persona";
                    }

                    int result = db.Execute(insertQuery, persona);

                    if (result > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Registro exitoso!";
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Error en el registro!";
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }

        public EntityBaseResponse obtenerPersona(int id)
        {

            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    var user = new EntityPersona();

                    //estado_registro = 1 exitoso
                    //estado_registro = 0 eliminado
                    //estado_registro = 2 pendiente registro sus datos

                    const string sql = "select * from persona where id_persona = @idpersona";

                    user = db.Query<EntityPersona>(sql, new { idpersona = id}).FirstOrDefault();

                    if (user != null)
                    {

                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = user;
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = "Usuario Incorrecto!";
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;

        }
    }
}
