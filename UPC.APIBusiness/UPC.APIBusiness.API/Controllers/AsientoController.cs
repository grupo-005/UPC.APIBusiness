﻿using DBContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API
{
    [Produces("application/json")]
    [Route("api/asiento")]
    public class AsientoController : Controller
    {
        protected readonly IAsientoRepository _asientoRepository;

        public AsientoController(IAsientoRepository asientoRepository) {
            _asientoRepository = asientoRepository;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("listar")]
        public ActionResult GetApartments()
        {
            var ret = _asientoRepository.GetAsientos();
            return Json(ret);
        }
    }
}
