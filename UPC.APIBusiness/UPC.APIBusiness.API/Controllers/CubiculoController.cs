﻿using DBContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{
    [Route("api/cubiculo")]
    [ApiController]
    public class CubiculoController : Controller
    {
        protected readonly ICubiculoRepository _cubiculoRepository;

        public CubiculoController(ICubiculoRepository cubiculoRepository)
        {
            _cubiculoRepository = cubiculoRepository;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("listar")]
        public ActionResult GetAll()
        {
            var ret = _cubiculoRepository.Listar();
            return Json(ret);
        }

    }
}
