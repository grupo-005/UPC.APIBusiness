﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityUsuario : EntityBase
    {
        public EntityUsuario(int idUsuario, int idPersona, string codUsuario)
        {
            id_usuario = idUsuario;
            id_persona = idPersona;
            cod_usuario = codUsuario;
           }
        public EntityUsuario() { 
        }
        public int id_usuario { get; set; }
        public int id_persona { get; set; }
        public string cod_usuario { get; set; }
        public string password { get; set; }
    }
}
