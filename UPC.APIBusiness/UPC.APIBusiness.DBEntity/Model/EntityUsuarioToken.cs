﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityUsuarioToken : EntityUsuario
    {
        public EntityUsuarioToken(EntityUsuario user, string token)
        : base(user.id_usuario, user.id_persona, user.cod_usuario)
        {
            this.token = token;
        }

        public EntityUsuarioToken() { 
        }

        public string token { get; set; }
    }
}
