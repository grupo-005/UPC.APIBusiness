﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using DBEntity;
using Dapper;

namespace DBContext
{
    public class LadoRepository : BaseRepository, ILadoRepository
    {
        public EntityBaseResponse Listar()
        {

            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    var asiento = new List<EntityLado>();
                    const string sql = "select * from lado";

                    asiento = db.Query<EntityLado>(sql).ToList();

                    if (asiento.Count > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = asiento;
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;

        }
    }
}
