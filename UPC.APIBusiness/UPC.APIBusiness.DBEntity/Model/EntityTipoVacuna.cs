﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityTipoVacuna: EntityBase
    {
        public int id_tipo_vacuna { get; set; }
        public string descripcion { get; set; }
    }
}
