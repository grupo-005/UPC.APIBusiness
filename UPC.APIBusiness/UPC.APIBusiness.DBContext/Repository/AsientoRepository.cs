﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using DBEntity;
using Dapper;
namespace DBContext
{
    public class AsientoRepository : BaseRepository, IAsientoRepository
    {
        public EntityBaseResponse GetAsientos()
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    var asiento = new List<EntityAsiento>();
                    const string sql = "select * from asiento";

                    asiento = db.Query<EntityAsiento>(sql).ToList();

                    if (asiento.Count > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = asiento;
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;

        }
    }
}
