﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityPiso: EntityBase
    {
        public int id_piso { get; set; }
        public string descripcion { get; set; }
        public string nomenclatura { get; set; }
        public string valor { get; set; }

    }
}
