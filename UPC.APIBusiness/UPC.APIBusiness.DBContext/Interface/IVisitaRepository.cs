﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface IVisitaRepository
    {
        EntityBaseResponse insertarVisita(EntityVisita visita);
        EntityBaseResponse validarRangoFecha(EntityVisitaRangofecha visita);

        EntityBaseResponse listarSolicitudes();

        EntityBaseResponse listarVisitasReporte(EntityVisitaReporteFiltro visita);

    }
}
