﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API
{
    [Produces("application/json")]
    [Route("api/persona")]
    [ApiController]
    public class PersonaController : Controller
    {
        protected readonly IPersonaRepository _personaRepository;

        public PersonaController(IPersonaRepository personaRepository) {
            _personaRepository = personaRepository;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("obtenerpersona")]
        public ActionResult obtenerpersona(int idpersona)
        {
            var ret = _personaRepository.obtenerPersona(idpersona);
            return Json(ret);
        }

        [Produces("application/json")]
        [Authorize]
        [HttpPost]
        [Route("registrarpersona")]
        public ActionResult registrarPersona(EntityPersona persona)
        {
            var ret = _personaRepository.insertPersona(persona);
            return Json(ret);
        }
    }
}
