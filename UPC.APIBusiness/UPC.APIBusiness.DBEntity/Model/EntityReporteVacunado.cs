﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityReporteVacunado
    {
        public int id_persona { get; set; }
        public string numero_documento { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public int id_registro_vacuna { get; set; }
        public int vacunado { get; set; }
        public int numero_dosis { get; set; }
        public string descripcion_otro_tipo { get; set; }
        public DateTime fecha_vacunacion { get; set; }
        public string lote { get; set; }
        public string descripcion { get; set; }
    }
}
