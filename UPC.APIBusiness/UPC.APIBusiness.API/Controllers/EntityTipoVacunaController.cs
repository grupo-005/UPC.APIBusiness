﻿using DBContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{
    [Produces("application/json")]
    [Route("api/tipovacuna")]
    public class EntityTipoVacunaController : Controller
    {
        protected readonly IEntityTipoVacunaRepository _entityTipoVacunaRepository;

        public EntityTipoVacunaController(IEntityTipoVacunaRepository interfaceCustom)
        {
            _entityTipoVacunaRepository = interfaceCustom;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("listar")]
        public ActionResult GetTipoVacunas()
        {
            var ret = _entityTipoVacunaRepository.GetTipoVacunas();
            return Json(ret);
        }
    }
}
