﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityBase
    {
        public int estado_registro { get; set; }
        public int usuario_crea { get; set; }
        public DateTime fecha_registro { get; set; }
        public int usuario_modifica { get; set; }
        public DateTime fecha_actualizacion { get; set; }

    }
}
