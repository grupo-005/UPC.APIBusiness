﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface IRegistroVacunaRepository
    {
        EntityBaseResponse insertRegistroVacuna(EntityRegistroVacuna vacuna);
        EntityBaseResponse obtenerRegistroVacuna(int id);
        EntityBaseResponse obtenerRegistroVacunaByPersona(int id);

        EntityBaseResponse listarReporte(EntityReporteVacunadoFiltro filtro);
    }
}
