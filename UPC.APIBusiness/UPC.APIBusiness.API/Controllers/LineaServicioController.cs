﻿using DBContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{
    [Produces("application/json")]
    [Route("api/lineaservicio")]
    [ApiController]
    public class LineaServicioController : Controller
    {
        protected readonly ILineaServicioRepository _LineaServicioRepository;

        public LineaServicioController(ILineaServicioRepository lineaServicioRepository) {
            _LineaServicioRepository = lineaServicioRepository;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("obtenerlineas")]
        public ActionResult obtenerLineas()
        {
            var ret = _LineaServicioRepository.GetLineasServicios();
            return Json(ret);
        }
    }
}
