﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Security;
using Newtonsoft.Json;

namespace API
{
    [Produces("application/json")]
    [Route("api/usuario")]
    [ApiController]
    public class UsuarioController : Controller
    {
        protected readonly IUsuarioRepository _iUsuarioRepository;

        public UsuarioController(IUsuarioRepository iUsuarioRepository) {
            _iUsuarioRepository = iUsuarioRepository;
        }

        [Produces("application/json")]
        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> login(EntityLogin login)
        {
            var ret = _iUsuarioRepository.GetUsuario(login.usuario, login.password);

            if (ret.issuccess) {

                var loginResponse = ret.data as EntityUsuario;

                var userId = loginResponse.id_usuario.ToString();

                var token = JsonConvert.DeserializeObject<AccessToken>(
                    await new Authentication().GenerateToken("", userId)
                    ).access_token;

                ret.data = new EntityUsuarioToken(loginResponse, token);
            }

            return Json(ret);
        }

    }
}
