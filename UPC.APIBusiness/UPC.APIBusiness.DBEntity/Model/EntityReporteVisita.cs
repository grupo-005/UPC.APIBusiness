﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityReporteVisita
    {
        public int id_visita { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string nombre_linea { get; set; }
        public string numero_documento { get; set; }
        public DateTime fecha_desde { get; set; }
        public DateTime fecha_hasta { get; set; }
        public int estado { get; set; }
        public int id_firma_espacio { get; set; }
        public int id_piso { get; set; }
        public string nomenclaturapiso { get; set; }
        public string descripcionpiso { get; set; }
        public int id_cubiculo { get; set; }
        public string nomenclaturacubiculo { get; set; }
        public string descripcioncubiculo { get; set; }
        public int id_lado { get; set; }
        public string nomenclaturalado { get; set; }
        public string descripcionlado { get; set; }
        public int id_asiento { get; set; }
        public string nomenclaturaasiento { get; set; }
        public string descripcionasiento { get; set; }
    }
}
