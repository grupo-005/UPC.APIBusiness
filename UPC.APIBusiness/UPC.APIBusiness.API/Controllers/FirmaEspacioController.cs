﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{
    [Route("api/espacio")]
    [ApiController]
    public class FirmaEspacioController : Controller
    {
        protected readonly IFirmaEspacioRepository _firmaEspacioRepository;

        public FirmaEspacioController(IFirmaEspacioRepository firmaEspacioRepository)
        {
            _firmaEspacioRepository = firmaEspacioRepository;
        }


        [Produces("application/json")]
        [Authorize]
        [HttpPost]
        [Route("registrar")]
        public ActionResult insertar(EntityFirmaEspacio dato)
        {
            var ret = _firmaEspacioRepository.registrar(dato);
            return Json(ret);
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("desasignar")]
        public ActionResult desasignar(int idVisita)
        {
            var ret = _firmaEspacioRepository.desasignar(idVisita);
            return Json(ret);
        }

    }
}
