﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityVisita: EntityBase
    {

        public int id_visita { get; set; }
        public int id_persona { get; set; }
        public int id_linea { get; set; }
        public DateTime fecha_desde { get; set; }
        public DateTime fecha_hasta { get; set; }
        public int estado { get; set; }


    }
}
