﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBContext;
using Microsoft.AspNetCore.Authorization;
using DBEntity;

namespace UPC.APIBusiness.API.Controllers
{
    [Produces("application/json")]
    [Route("api/visita")]
    [ApiController]
    public class VisitaController : Controller
    {
        protected readonly IVisitaRepository _visitaRepository;

        public VisitaController(IVisitaRepository visitaRepository) {
            _visitaRepository = visitaRepository;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpPost]
        [Route("registrarvisita")]
        public ActionResult registrarVisita(EntityVisita visita)
        {
            var ret = _visitaRepository.insertarVisita(visita);
            return Json(ret);
        }

        [Produces("application/json")]
        [Authorize]
        [HttpPost]
        [Route("validarrangofecha")]
        public ActionResult validarRangoFecha(EntityVisitaRangofecha visita)
        {
            var ret = _visitaRepository.validarRangoFecha(visita);
            return Json(ret);
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("listarsolicitudasignacion")]
        public ActionResult listarsolicitudasignacion( )
        {
            var ret = _visitaRepository.listarSolicitudes();
            return Json(ret);
        }



        [Produces("application/json")]
        [Authorize]
        [HttpPost]
        [Route("listarreporte")]
        public ActionResult listarReporte(EntityVisitaReporteFiltro visita)
        {
            var ret = _visitaRepository.listarVisitasReporte(visita);
            return Json(ret);
        }

    }
}
