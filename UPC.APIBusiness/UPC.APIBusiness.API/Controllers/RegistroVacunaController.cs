﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{
    [Produces("application/json")]
    [Route("api/registrovacuna")]
    [ApiController]
    public class RegistroVacunaController : Controller
    {

        protected readonly IRegistroVacunaRepository _registroVacunaRepository;

        public RegistroVacunaController(IRegistroVacunaRepository registroVacunaRepository)
        {
            _registroVacunaRepository = registroVacunaRepository;
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("obtenervacunapersona")]
        public ActionResult obtenerRegistroVacunaByPersona(int idpersona)
        {
            var ret = _registroVacunaRepository.obtenerRegistroVacunaByPersona(idpersona);
            return Json(ret);
        }

        [Produces("application/json")]
        [Authorize]
        [HttpGet]
        [Route("obtenervacuna")]
        public ActionResult obtenerRegistroVacuna(int idvacuna)
        {
            var ret = _registroVacunaRepository.obtenerRegistroVacuna(idvacuna);
            return Json(ret);
        }

        [Produces("application/json")]
        [Authorize]
        [HttpPost]
        [Route("registrarvacuna")]
        public ActionResult insertRegistroVacuna(EntityRegistroVacuna registrovacuna)
        {
            var ret = _registroVacunaRepository.insertRegistroVacuna(registrovacuna);
            return Json(ret);
        }

        [Produces("application/json")]
        [Authorize]
        [HttpPost]
        [Route("listarreporte")]
        public ActionResult listarReporte(EntityReporteVacunadoFiltro filtro)
        {
            var ret = _registroVacunaRepository.listarReporte(filtro);
            return Json(ret);
        }

    }
}
