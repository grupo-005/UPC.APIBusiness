﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class EntityTipoVacunaRepository : BaseRepository, IEntityTipoVacunaRepository
    {
        public EntityBaseResponse GetTipoVacunas()
        {
            
                var response = new EntityBaseResponse();

                try
                {
                    using (var db = GetSqlConnection())
                    {
                        var asiento = new List<EntityTipoVacuna>();
                        const string sql = "select * from tipo_vacuna";

                        asiento = db.Query<EntityTipoVacuna>(sql).ToList();

                        if (asiento.Count > 0)
                        {
                            response.issuccess = true;
                            response.errorcode = "0000";
                            response.errormessage = string.Empty;
                            response.data = asiento;
                        }
                        else
                        {
                            response.issuccess = false;
                            response.errorcode = "0000";
                            response.errormessage = string.Empty;
                            response.data = null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    response.issuccess = false;
                    response.errorcode = "0001";
                    response.errormessage = ex.Message;
                    response.data = null;
                }

                return response;
            }
    }
}
