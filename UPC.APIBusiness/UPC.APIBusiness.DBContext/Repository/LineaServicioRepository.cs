﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class LineaServicioRepository : BaseRepository, ILineaServicioRepository
    {
        public EntityBaseResponse GetLineasServicios()
        {

            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    var lineaServicio = new List<EntityLineaServicio>();
                    const string sql = "select * from linea_servicio";

                    lineaServicio = db.Query<EntityLineaServicio>(sql).ToList();

                    if (lineaServicio.Count > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = lineaServicio;
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;

        }
    }
}
