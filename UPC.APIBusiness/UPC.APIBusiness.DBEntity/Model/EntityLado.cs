﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityLado : EntityBase
    {
        public int id_lado { get; set; }
        public string descripcion { get; set; }
        public string nomenclatura { get; set; }
        public string valor { get; set; }
    }
}
