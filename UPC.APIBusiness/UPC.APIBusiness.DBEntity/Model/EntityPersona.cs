﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityPersona: EntityBase
    {
        public int id_persona { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string celular { get; set; }
        public string numero_documento { get; set; }
        public DateTime fecha_nacimiento { get; set; }
        public string direccion { get; set; }
    }
}
