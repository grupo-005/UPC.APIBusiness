﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class UsuarioRepository : BaseRepository, IUsuarioRepository
    {
        public EntityBaseResponse GetUsuario(string usuario, string password)
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {
                    var user = new EntityUsuario();

                    const string sql = "select * from usuario where cod_usuario = @usu AND password  = @pass";

                    user = db.Query<EntityUsuario>(sql, new { usu = usuario, pass = password }).FirstOrDefault();

                    if (user != null)
                    {
                        user.password = null;
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = user;
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = "Usuario Incorrecto!";
                        response.data = null;
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }
    }
}
