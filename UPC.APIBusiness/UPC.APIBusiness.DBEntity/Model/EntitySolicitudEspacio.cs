﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntitySolicitudEspacio
    {
        public int id_visita { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string nombre_linea { get; set; }
        public string numero_documento { get; set; }
        public DateTime fecha_desde { get; set; }
        public DateTime fecha_hasta { get; set; }
        public int estado { get; set; }
        public int id_firma_espacio { get; set; }


    }
}
