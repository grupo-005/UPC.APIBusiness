﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface IPersonaRepository
    {
        EntityBaseResponse insertPersona(EntityPersona persona);
        EntityBaseResponse obtenerPersona(int id);
    }
}
