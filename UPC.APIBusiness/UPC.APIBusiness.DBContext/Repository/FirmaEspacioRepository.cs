﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DBContext
{
    public class FirmaEspacioRepository : BaseRepository, IFirmaEspacioRepository
    {
        public EntityBaseResponse desasignar(int idVisita)
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {

                    string updateVisitaQuery = @"UPDATE [dbo].[registro_visita] SET [estado]=0 WHERE [id_visita]=@idVisita";
                    int updateVisita = db.Execute(updateVisitaQuery, new { idVisita });

                    string updateFirmaQuery = @"UPDATE [dbo].[firma_espacio] SET [estado_registro]=0 WHERE [id_visita]=@idVisita";
                    int updateFirma = db.Execute(updateFirmaQuery, new { idVisita });


                    if (updateVisita > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Registro exitoso!";
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Error en el registro!";
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }

        public EntityBaseResponse registrar(EntityFirmaEspacio espacio)
        {
            var response = new EntityBaseResponse();

            try
            {
                using (var db = GetSqlConnection())
                {

                    string insertQuery = @"INSERT INTO [dbo].[firma_espacio]([id_piso], [id_cubiculo], [id_asiento], [id_lado], [estado_registro],[fecha_registro], [usuario_crea], [id_visita]) VALUES (@id_piso, @id_cubiculo, @id_asiento, @id_lado, @estado_registro, @fecha_registro, @usuario_crea, @id_visita)";

                    string updateQuery = @"UPDATE [dbo].[registro_visita] SET [estado]=@estado WHERE [id_visita]=@id_visita";
                    var estado = 1;
                    int update = db.Execute(updateQuery, new { estado, espacio.id_visita });

                    int result = db.Execute(insertQuery, espacio);

                    if (result > 0)
                    {
                        response.issuccess = true;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Registro exitoso!";
                    }
                    else
                    {
                        response.issuccess = false;
                        response.errorcode = "0000";
                        response.errormessage = string.Empty;
                        response.data = "Error en el registro!";
                    }
                }
            }
            catch (Exception ex)
            {
                response.issuccess = false;
                response.errorcode = "0001";
                response.errormessage = ex.Message;
                response.data = null;
            }

            return response;
        }
    }
}
